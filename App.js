import React from 'react';
import addInsurance from './components/addInsurance';
import welcome from './components/welcome';
import {
  NavigatorIOS,
  ActivityIndicator,
} from 'react-native';
import { Font } from 'expo';
import { niceGreen, background } from './components/generalStyles';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
    };
    // AsyncStorage.clear(); If needed to clear in development
  }

  async componentWillMount() {
    await Font.loadAsync({
      'FontAwesome': require('./assets/fonts/fontawesome-webfont.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  _handleAddInsurancePress() {
    this.props.navigator.push({
      component: addInsurance,
      passProps: {
        back: this.refs.nav.pop
      },
      barTintColor: background,
      tintColor: '#fff',
      leftButtonTitle: 'Cancel',
      onLeftButtonPress: this.refs.nav.pop,
      navigationBarHidden: false
    })
  };

  render() {
    if (this.state.fontLoaded) {
      return (
        <NavigatorIOS
          ref='nav'
          navigationBarHidden={true}
          initialRoute={{
            title: '',
            component: welcome,
            passProps: {
              currentItems: this.state.data,
              addFunction: this._handleAddInsurancePress
            },
          }}
          style={{flex: 1}}
        />);
    } else {
      return (
        <ActivityIndicator size='large' color={niceGreen} style={{flex: 1}}/>
      );
    }
  }
}
