import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, Button, ListView, AsyncStorage } from 'react-native';
import { generalStyles } from './generalStyles';
import CurrentPolicies from './currentPolicies';
import FontAwesome, { Icons } from 'react-native-fontawesome';

export default class welcome extends Component {
  render() {
    return (
      <View style={generalStyles.container}>
        <Text style={generalStyles.titleText}>My Insurance</Text>
        <Text style={generalStyles.sum}>{}</Text>
        <CurrentPolicies/>
        <View style={generalStyles.bottom}>
          <Text onPress={this.props.addFunction}>
            <FontAwesome style={generalStyles.add}>{Icons.plusCircle}</FontAwesome>
          </Text>
        </View>
      </View>
    );
  }
}
