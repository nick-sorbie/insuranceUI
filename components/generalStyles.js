import { StyleSheet } from 'react-native';
import { Constants } from 'expo';

export const niceGreen = '#59c159';
export const red = '#ff5e5e';
export const background = '#232933';

export const selectOptions = {
    marginTop: -50
};

export const generalStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    paddingBottom: 130,
    backgroundColor: background,
    paddingTop: 60,
  },
  titleText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 30,
    color: '#fff'
  },
  addTitleText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 25,
    color: '#fff',
    paddingBottom: 10,
  },
  formView: {
    opacity: 0.5,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingBottom: 10,
  },
  listView: {
    marginTop: 30,
  },
  inputForm: {
    backgroundColor: '#fff',
    width: 320,
    height: 40,
    padding: 8,
    marginBottom: 8,
    paddingTop:10,
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2,
    borderBottomLeftRadius: 2,
    borderBottomRightRadius: 10,
  },
  rowItem: {
    marginTop: 5,
    alignItems: 'center',
    padding: 5,
    width: 320,
    backgroundColor: '#fff',
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2,
    borderBottomLeftRadius: 2,
    borderBottomRightRadius: 10,
    flex: 1,
    flexDirection: 'row',
  },
  rowText: {
    flex: 4,
    flexDirection: 'column',
  },
  rowPrice: {
    flex: 3,
    flexDirection: 'column',
    color: "#bbb"
  },
  rowRemove: {
    flex: 1,
    flexDirection: 'column',
    textAlign: "right",
    paddingRight: 5,
  },
  sum: {
    color: red,
    textAlign: "center",
    flex: 0,
    fontSize: 20,
    fontStyle: "italic",
    alignContent: "flex-start",
    padding: 5,
    marginBottom: -10
  },
  remove: {
    fontSize: 15,
    color: red,
  },
  bottom:{
    position: 'absolute',
    bottom: 20,
    backgroundColor: background,
  },
  add: {
    flexDirection: "column",
    fontSize: 70,
    color: niceGreen,
    flex: 1
  }
});