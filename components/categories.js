import { StyleSheet } from 'react-native';
import { background } from './generalStyles';

export const categories = [
    'home',
    'vehicle',
    'travel',
    'life',
    'medical',
    'pet'
];

const additional = {
    borderLeftWidth:0,
    borderRightWidth:0,
    borderTopWidth:3,
    borderBottomWidth:0,
    padding:10
};

export const categoryStyles = StyleSheet.create({
    home: {
        borderColor: '#D2691E',
        ...additional
    },
    vehicle: {
        borderColor: '#672E3B',
        ...additional
    },
    travel: {
        borderColor: '#9C9A40',
        ...additional
    },
    life: {
        borderColor: '#DC4C46',
        ...additional
    },
    medical: {
        borderColor: '#005960',
        ...additional
    },
    pet: {
        borderColor: '#C48F65',
        ...additional
    },
});