import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Button,
  ListView,
  AsyncStorage,
  NavigatorIOS,
  Picker } from 'react-native';
import { generalStyles, selectOptions } from './generalStyles';
import {categories} from './categories';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import welcome from './welcome';

const show = Object.assign({}, selectOptions, { display: "flex" }),
hide = Object.assign({}, selectOptions, { display: "none"});

export default class addInsurance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      amount: '',
      category: '',
      inputStyles: hide,
    };
    this._handleSubmitButtonPress = this._handleSubmitButtonPress.bind(this);
  }

  async _handleSubmitButtonPress() {
    if (this.state.title === '' || this.state.amount === null || this.state.category === '') {
      return;
    }
    try {
      const storedItems = await AsyncStorage.multiGet(['policies', 'policySum']);
      const policies = storedItems[0][1], 
      policySum = storedItems[1][1];
      if(!policies || !policySum) {
        AsyncStorage.multiSet([
          ['policies', JSON.stringify([this.state])],
          ['policySum', JSON.stringify(this.state.amount)]
        ] , this.props.back);
      } else {
        const update = JSON.parse(policies), currentAmount = parseFloat(JSON.parse(policySum)),
        newAmount =  parseFloat(this.state.amount),
        newSum = currentAmount + newAmount;
        update.push(this.state);
        AsyncStorage.multiSet([
          ['policies', JSON.stringify(update)],
          ['policySum', JSON.stringify(newSum)]
        ] , this.props.back);
      }
    } catch (error) {
      console.log(error);
    }
  };

  _capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    return (
      <View style={generalStyles.container}>
          <Text style={generalStyles.addTitleText}>Add</Text>
          <TextInput
            style={generalStyles.inputForm}
            id="title"
            value={this.state.title}
            onChangeText={(text) => this.setState({'title':text})}
            placeholder="Title"
          />
          <TextInput
            style={generalStyles.inputForm}
            id="amount"
            keyboardType="numeric"
            value={this.state.amount === '' ? '' : `€${this.state.amount}`}
            onChangeText={(text) => {
              const noText = text.replace(/[^0-9.]/g, '');
              this.setState({'amount': noText})
            }}
            placeholder="0.00"
          />
          <View style={generalStyles.inputForm}>
            <Text onPress={() => this.setState({'inputStyles': show})}>
              {this.state.category === '' ? 'Category' : this._capitalizeFirstLetter(this.state.category)}
            </Text>
            <Picker
              id="category"
              style={this.state.inputStyles}
              selectedValue={this.state.category === '' ? 0 : this.state.category}
              onValueChange={(itemValue, itemIndex) => {
                  this.setState({
                    category: itemValue,
                    inputStyles: hide
                  })
                }
              }>
              {
                categories.map((name, key) => {
                  return (
                  <Picker.Item
                    style={generalStyles.inputSelectOptions}
                    key={key}
                    label={this._capitalizeFirstLetter(name)}
                    value={name}
                    color="#fff"
                    />)
                })
              }
            </Picker>
          </View>
          <View style={generalStyles.bottom}>
            <Text onPress={this._handleSubmitButtonPress}>
              <FontAwesome style={generalStyles.add}>{Icons.plusCircle}</FontAwesome>
            </Text>
          </View>
      </View>);
  }
}
