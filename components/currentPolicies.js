import React, { Component } from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    ListView,
    View,
    Text,
    Alert
} from 'react-native';
import { niceGreen, generalStyles } from './generalStyles';
import { categoryStyles } from './categories';
import FontAwesome, { Icons } from 'react-native-fontawesome';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class CurrentPolicies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listLoaded: false,
            keys: [],
            dataSource: null,
            policySum: 0,
        };
        this._handleDeleteButtonPress = this._handleDeleteButtonPress.bind(this);
      }

    async _getCurrentPolicies() {

        const storedItems = await AsyncStorage.multiGet(['policies', 'policySum']);
        // not proud of this but multiget returns like so:
        // multiGet(['k1', 'k2'], cb) -> cb([['k1', 'val1'], ['k2', 'val2']])
        const policies = storedItems[0][1], 
        policySum = storedItems[1][1];

        if (!policies || !policySum) {
          return this.setState({
            'listLoaded' : true,
            dataSource: ds.cloneWithRows([])
            });
        }
        return this.setState({
          'listLoaded' : true,
          dataSource: ds.cloneWithRows(JSON.parse(policies)),
          policySum: JSON.parse(policySum)
        });
    }

    componentWillReceiveProps() {
        this._getCurrentPolicies();
    }

    componentDidMount(){
      if (!this.state.listLoaded) {
          this._getCurrentPolicies();
      }
    }

    _handleDeleteButtonPress = (id, rowData) => {
      Alert.alert(
        'Confirm',
        `You are about to delete '${rowData.title}'`,
        [
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
          {text: 'Continue', onPress: () => {
            const newAmount = this.state.policySum - parseFloat(rowData.amount),
            newItems = this.state.dataSource._dataBlob.s1.filter((item, i) => (parseInt(id) !== i));

            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(newItems),
                policySum: newAmount
            });
            AsyncStorage.multiSet([
              ['policies', JSON.stringify(newItems)],
              ['policySum', JSON.stringify(newAmount)]
            ]);
          }},
        ],
      )
    };
    
    _moneyFormat(number) {
      return `€${parseFloat(Math.round(number * 100) / 100).toFixed(2)}`;
    }
    
    render() {
        let i = 0;
        if(this.state.listLoaded) {
        return(
          <View>
            <Text style={generalStyles.sum}>{this._moneyFormat(this.state.policySum)}</Text>
          <ListView
            enableEmptySections={true}
            style={generalStyles.listView}
            dataSource={this.state.dataSource}
            renderRow={(rowData, sectionID, rowID) => {
              const handleDelete = () => {
                return this._handleDeleteButtonPress(rowID, rowData);
              }
              return (
                <View style={[generalStyles.rowItem, categoryStyles[rowData.category]]}>
                  <Text style={generalStyles.rowText}>{rowData.title}</Text>
                  <Text style={generalStyles.rowPrice}>{
                    this._moneyFormat(rowData.amount)
                    }</Text>
                  <Text style={generalStyles.rowRemove} onPress={handleDelete}>
                    <FontAwesome style={generalStyles.remove}>{Icons.trash}</FontAwesome>
                  </Text>
                </View>
              );
            }}
          />
          </View>
        )
        } else {
          return (
            <ActivityIndicator size="large" color={niceGreen} style={{flex: 1}}/>
          );
        }
    }
  }